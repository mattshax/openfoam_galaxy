#! /bin/bash
# set -x

usage="$0 --input inputfile --mesh meshtemplatefile --outvtk tarfileofvtks --params name value ..."

echo $usage

echo $@

while [ $# -gt 0 ]; do
  case $1 in
    --input) input=$2; shift 2;;
    --mesh) mesh=$2; shift 2;;
    --outvtk) outvtk=$2; shift 2;;
    --params) shift 1;
        while [ $# -gt 0 ] && ! echo $1 | grep -q -- -- ; do
          pname=$1
          pval=$2
          echo $pname
          echo $pval
          shift 2
          sed -i -e "s/@@$pname@@/$pval/g" $mesh 
        done
        ;;
    *) echo $usage 1>&2
     exit 1;;
  esac
done

# unzip the galaxy input files - this needs some work
cp $input ./;tar xvzf $(basename $input) --strip-components=5;tar xvf $(basename $input .tgz) --strip-components=1

# put the mesh file into the polymesh directory
cp $mesh $(pwd)/constant/polyMesh/blockMeshDict

# run openfoam
. /opt/openfoam231/etc/bashrc

blockMesh #> $PWD/results/blockMesh_log.txt
simpleFoam #> $PWD/results/simpleFoam_log.txt
foamToVTK -ascii #> $PWD/results/foamToVTK_log.txt

# move the result files
#pwdsave=$PWD;cd $pwdsave/VTK;
#mv * $pwdsave/results;cd $pwdsave;

RC=$?

if [ $RC = 0 ]; then

  echo OpenFoam application completed: RC=0
  
  # upload these files to S3
  s3_url=http://parallelworksgalaxy.s3-website-us-west-1.amazonaws.com/openfoam
  echo "Uploading files to "$s3_url
  
  outvtkS3tgz=$(basename $outvtk .txt).tgz
    
  # only tar up the VTK files
  tar zcf $outvtkS3tgz VTK
  
  # move the resulting tar file to the swift output
  $(pwd)/putS3 $outvtkS3tgz openfoam
  echo $s3_url/$outvtkS3tgz > $outvtk
  
else
  echo OpenFoam application failed: RC=$RC
fi

#tar zcf $outall *

exit $RC
