#! /bin/sh

echo "Running OpenFOAM Swift Workflow in Parallel.Works";echo "";

# extract run parameters
SWEEP_PARAMS=$*;
while [ $# -gt 0 ]; do
  case $1  in
    *-poolid*)  poolid=$(echo $1 | sed 's/-poolid=/\n/g' | sed '/^$/d'); echo PoolID: $poolid; shift ;;
    *-uiport*)  uiport=$(echo $1 | sed 's/-uiport=/\n/g' | sed '/^$/d'); echo Coaster Port: $uiport; shift ;;
    *-sweep*)  sweep=$(echo $1 | sed 's/-sweep=/\n/g'); echo Sweep: $sweep; shift ;;
    *-input*)  input=$(echo $1 | sed 's/-input=/\n/g'); echo Input: $input; shift ;;
    *-mesh*)  mesh=$(echo $1 | sed 's/-mesh=/\n/g'); echo Mesh: $mesh; shift ;;
    *-results*)  results=$(echo $1 | sed 's/-results=/\n/g'); echo Results: $results; shift ;;
    *) echo "no match"; shift ;;
  esac
done

# config settings
export SWIFT_HEAP_MAX=4G;export SWIFT_USERHOME=$dir/swifthome

# get the repo files swift.conf file - overright swift.conf coaster port - THIS NEEDS WORK!
dir=$(cd $(dirname $0); /bin/pwd); cp $dir/* ./;SED_ARG="-i s/@@PORT@@/$poolid/ swift.conf";eval sed "$SED_ARG"

# generate the expanded sweep file - this can get WAY better with shell!
new_sweep=$(basename $(node presweep.js $sweep));echo Sweep: $new_sweep;sweepname=$(basename $sweep)
NEW_SWEEP_PARAMS=$(echo $SWEEP_PARAMS | sed -e "s|$sweepname|$new_sweep|")

# temporary in case not defined
uiport=3018

# TODO - make the -ui PORT variable based on an input from galaxy form
# run the energyplus sweep via swift
echo "swift -ui http:$uiport openfoamsweep.swift $NEW_SWEEP_PARAMS"
swift -ui http:$uiport openfoamsweep.swift $NEW_SWEEP_PARAMS

# create the summary result files for Galaxy
echo "Generating Output Files for Galaxy"
outdir=$(pwd)/results

# VTK RESULT FILE - TODO - GENERATE HTML FILE - ONCLICK OPEN PW SLIDER PARAVIEWWEB OF RESULTS
vtkTmp=$outdir/vtk.html
for f in $outdir/vtk/*.txt; do
  link=$(cat $f)
  echo "<a href='javascript:void(0)' onclick=window.top.openParaView('$link')>" >> $vtkTmp
  echo $(basename $f .txt)" Results" >> $vtkTmp
  echo "</a><br>" >> $vtkTmp
done
cp $vtkTmp $results

echo "process complete"
