
# ----- TYPE DEFINITIONS

  type file;
  
  type param {
    string pname;
    string pvals;
  }
  
  type pval {
    string name;
    string[] values;
  }


# ----- APP DEFINITIONS

  app (file vtk) runIteration ( file docker, file putS3, file execute, file input, file mesh, string params[] )
  {
    bash @docker "mattshax/openfoam_paraview_mesa" @execute "--input" @input "--mesh" @mesh "--outvtk" @vtk "--params" params;
  }


# ----- EXTERNAL INPUTS
  
  file mesh <single_file_mapper; file=arg("mesh","blockMeshDict_template")>;
  file execute <single_file_mapper; file=arg("execute","RunAndReduceOpenFoam.sh")>;
  file input <single_file_mapper; file=arg("input","input")>;
  
  string outdir=arg("outdir","results");
  string count=arg("count","false");

  param  pset[] = readData(arg("sweep","sample.sweep"));


# ----- INTERNAL FUNCTIONS

  (string[] r) extend(string[] a, string v,string n, int lastIndex) {
    foreach ov, i in a {r[i] = a[i];}
    string[] f;f[0]=n;f[1]=v;
    r[lastIndex]=@strjoin(f,",");
  }
  
  iterateOF(file ex1, file in1, file c, string[] parameters, string d) {
    string ap=@strjoin(parameters,",");
    string[] apx=@strsplit(ap,",");
    string[] aval;
    foreach v, vn in parameters {
      string[] an=@strsplit(v,",");
      aval[vn]=an[1];
    }
    string fileid = @strcat("openfoam_",@strjoin(aval,"_"));
    tracef("%s\n",fileid);
    
    file docker <single_file_mapper; file="docker_run">;
    file putS3 <single_file_mapper; file="putS3">;
    file outvtk <single_file_mapper; file=@strcat(d,"/vtk/",fileid,".txt")>;  
    (outvtk) = runIteration (docker,putS3,ex1,in1,c,apx);
    
  }

  dynamicFor(pval[] vars, string[] values, int index, int len, file exc, file inc, file mc, string od) {
    if (index < len) {
      foreach v in vars[index].values {dynamicFor(vars, extend(values, v,vars[index].name, index), index + 1, len, exc, inc, mc, od);}
    }else {iterateOF(exc,inc,mc,values,od);}
  }

  (int sz) countAllRecursive(pval[] pval, int index) {
    if (index < 0) {sz = 1;}
    else {sz = @length(pval[index].values) * countAllRecursive(pval, index - 1);}
  }

  (int sz) countAll(pval[] pval) {
    if (@length(pval) == 0) {sz = 0;}
    else {sz = countAllRecursive(pval, @length(pval) - 1);}
  }


# ----- PARAMETER SETUP & LAUNCH
  
  string[] empty;
  pval[] pvals;

  foreach p, pn in pset {
      string val[]=@strsplit(p.pvals,",");
      pvals[pn].name=p.pname;
      foreach v, vn in val {
        pvals[pn].values[vn]=v;
      }
  }
  
  int runs=countAll(pvals);
  tracef("%i Runs in Simulation\n\n",runs);
  
  if (count=="false"){
      dynamicFor(pvals, empty, 0, @length(pvals), execute, input, mesh, outdir);
  }

