var fs = require('fs');

var fileName = process.argv[2];

fs.readFile(fileName, "utf-8", function(err, data) {
    if (err) throw err;
    data = data.split("\n");
    var datanew = [];
    for (var i = 0; i < data.length; i++) {
        if (data[i].indexOf(":") > -1) {
            var dataIt = data[i].split(/\s+/);
            var rep = dataIt[1].split(":");
            var newVal = [];
            for (j = parseFloat(rep[0]); j <= parseFloat(rep[1]); j = j + parseFloat(rep[2])) {
                newVal.push(j);
            }
            newVal.join(",");
            var newData = [dataIt[0], newVal];
            newData = newData.join('  ');
            datanew.push(newData);
        }
        else {
            datanew.push(data[i]);
        }
    }
    var datanew = datanew.join("\n");
    fs.writeFile(fileName.split(".")[0] + "_run.sweep", datanew, function(err) {
        if (err) {
            console.log(err);
        }
        else {
            console.log(fileName.split(".")[0] + "_run.sweep");
        }
    });

});
